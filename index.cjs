const http = require('http');
const fs = require('fs');
const path = require("path");
const uuid=require('uuid');


const server = http.createServer((request, response) => {
    const statusCode = request.url.split('/')[2];
    const delaySecond = request.url.split('/')[2];
    if (request.url === '/html'&& request.method === 'GET') {
        let paths = path.join(__dirname, "index.html")
        fs.readFile(paths, 'utf-8', (error, data) => {
            if (error) {
                response.writeHead(404);
                response.write("Error occured While reading html File");
            } else {
                response.writeHead(200, { "Content-Type": "text/html" });
                response.write(data)
            }
            response.end();
        })
    }else if(request.url === '/json' && request.method === 'GET'){
        let paths = path.join(__dirname, "data.json")
        fs.readFile(paths, 'utf-8', (error, data) => {
            if (error) {
                response.writeHead(404);
                response.write("Error occured While reading json File");
            } else {
                response.writeHead(200, { "Content-Type": 'application/json' });
                response.write(data)
            }
            response.end();
        })
    }else if(request.url === '/uuids' && request.method === 'GET'){
        const uuid4 = uuid.v4()
        response.writeHead(200,{'Content-Type':'application/json'})
        response.write(JSON.stringify(`{ uuid : ${uuid4} }`));
        response.end();
    }else if (request.url === `/status/${statusCode}` && request.method === 'GET') {
        if (!isNaN(statusCode) && http.STATUS_CODES[statusCode]) {
            response.writeHead(statusCode, { 'Content-Type': 'application/json' });
            
            response.write(`Return a response with ${statusCode} status code`);
            response.end();
        }
        else {
            response.writeHead(400, { 'Content-Type': 'application/json' })
            const errorMessage = {
                message: "Status code is not valid"
            }
            response.write(JSON.stringify(errorMessage));
            response.end();
        }
    }else if (request.url === `/delay/${delaySecond}` && request.method === 'GET') {
        if (isNaN(delaySecond)) {
            request.writeHead(400, { 'Content-Type': 'application/json' });
            const errorMessage = {
                message: "Second is not a number"
            }
            response.write(JSON.stringify(errorMessage));
            response.end();
        }
        else {
            setTimeout(() => {
                response.writeHead(200, { 'Content-Type': 'application/json'});
                const delayTimes = {
                    'delayInseconds': delaySecond
                };
                response.write(JSON.stringify(delayTimes));
                response.end();
            }, delaySecond* 1000);
        }
    }
})
server.listen(5000, function (error) {
    if (error) {
        console.log("an error occured" + error)
    } else {
        console.log("Server started at " + 5000)
    }
})


